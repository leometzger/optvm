package optvm;

import optvm.objectives.Objective;
import org.moeaframework.core.Solution;
import org.moeaframework.core.Variable;
import org.moeaframework.core.variable.EncodingUtils;
import org.moeaframework.core.variable.RealVariable;
import org.moeaframework.problem.AbstractProblem;
import optvm.entities.VM;

import java.util.List;

public class OptvmProblem extends AbstractProblem {

    private List<Objective> objectives;

    private Environment environment;

    public OptvmProblem(List<Objective> objectives, Environment environment) {
        super(10, objectives.size());
        this.objectives = objectives;
        this.environment = environment;
    }

    @Override
    public Solution newSolution() {
        Solution solution = new Solution(numberOfVariables, objectives.size());


        return solution;
    }

    @Override
    public void evaluate(Solution solution) {
        double[] variables = EncodingUtils.getReal(solution);

        // Transform solution to domain objects
        // Add domain objects to Context
        // Evaluate functions based on context
        // Set objective values
    }
}

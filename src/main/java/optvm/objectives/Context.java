package optvm.objectives;

import optvm.entities.Datacenter;
import optvm.entities.Environment;
import optvm.entities.Host;
import optvm.entities.VM;

public class Context {

    private Datacenter datacenter;

    private Host host;

    private VM vm;

    public Context(Datacenter datacenter, Host host, VM vm) {
        this.datacenter = datacenter;
        this.host = host;
        this.vm = vm;
    }

    public Datacenter getDatacenter() {
        return datacenter;
    }

    public void setDatacenter(Datacenter datacenter) {
        this.datacenter = datacenter;
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public VM getVm() {
        return vm;
    }

    public void setVm(VM vm) {
        this.vm = vm;
    }
}

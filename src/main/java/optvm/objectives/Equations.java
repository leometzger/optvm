package optvm.objectives;

import optvm.entities.VM;
import optvm.entities.Host;

import java.util.List;

public class Equations {

    public double calculateTraffic(Host host, VM vm, double processingFactor) {
        return (vm.getSize() / host.getHops()) + (host.getHops() * processingFactor);
    }

    public double calculateEnergy(List<Host> hosts) {
         double result = 0;

        for(Host host : hosts) {
            if(host.isActive()) {
                result += ((host.getPmax() - host.getPmin()) * host.getCpu() * host.getPmin());
            }
        }
        return result;
    }

    public double calculateVolume(Host host) {
        return (1 / (1 - host.getCcap())) *
                (1 / (1 - host.getCbw())) *
                (1 / (1 - host.getCram())) *
                (1 / (1 - host.getCsth()));
    }

    public double calculateCapacity(Host host) {
        return host.getProcessingUnits() * host.getMips();
    }

    public double calculateFixationTime(Host host) {
        double memUsed = 0;

        for(VM vm : host.getVmActive()) {
            memUsed += vm.getCurrentAllocatedRam();
        }

        for(VM vm : host.getVmMig()) {
            memUsed += vm.getCurrentAllocatedRam();
        }
        return memUsed / host.getBw();
    }
}

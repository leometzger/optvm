package optvm.objectives;

public class SelectionPerformance implements Objective {

    private Equations equations;

    public SelectionPerformance(Equations equations) {
       this.equations = equations;
    }

    @Override
    public double execute(Context context) {
        return this.equations.calculateVolume(context.getHost());
    }
}

package optvm.objectives;

public interface Objective {

    double execute(Context context);
}

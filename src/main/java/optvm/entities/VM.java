package optvm.entities;

import org.moeaframework.core.Variable;

import java.util.List;
import java.util.Random;

public class VM implements Variable {

    private int pe;

    private long storage;

    private long ram;

    private int dirdyPages;

    private long size;

    private double mips;

    private int numberOfPes;

    private long bw;

    private String vmm;

    private long currentAllocatedSize;

    private long currentAllocatedRam;

    private long currentAllocatedBw;

    private List<Double> currentAllocatedMips;

    private boolean beingInstantiated;

    private Host host;

    private Datacenter datacenter;

    public VM() {
        this.pe = 0;
        this.mips = 0;
        this.storage = 0;
        this.ram = 0;
    }

    public Datacenter getDatacenter() {
        return datacenter;
    }

    public void setDatacenter(Datacenter datacenter) {
        this.datacenter = datacenter;
    }

    public long getMigrationTime() {
        return this.bw * this.ram;
    }

    public int getPe() {
        return pe;
    }

    public void setPe(int pe) {
        this.pe = pe;
    }

    public double getMips() {
        return mips;
    }

    public void setMips(long mips) {
        this.mips = mips;
    }

    public long getStorage() {
        return storage;
    }

    public void setStorage(long storage) {
        this.storage = storage;
    }

    public long getRam() {
        return ram;
    }

    public void setRam(long ram) {
        this.ram = ram;
    }

    public int getDirdyPages() {
        return dirdyPages;
    }

    public void setDirdyPages(int dirdyPages) {
        this.dirdyPages = dirdyPages;
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public void setMips(double mips) {
        this.mips = mips;
    }

    public int getNumberOfPes() {
        return numberOfPes;
    }

    public void setNumberOfPes(int numberOfPes) {
        this.numberOfPes = numberOfPes;
    }

    public long getBw() {
        return bw;
    }

    public void setBw(long bw) {
        this.bw = bw;
    }

    public String getVmm() {
        return vmm;
    }

    public void setVmm(String vmm) {
        this.vmm = vmm;
    }

    public long getCurrentAllocatedSize() {
        return currentAllocatedSize;
    }

    public void setCurrentAllocatedSize(long currentAllocatedSize) {
        this.currentAllocatedSize = currentAllocatedSize;
    }

    public long getCurrentAllocatedRam() {
        return currentAllocatedRam;
    }

    public void setCurrentAllocatedRam(long currentAllocatedRam) {
        this.currentAllocatedRam = currentAllocatedRam;
    }

    public long getCurrentAllocatedBw() {
        return currentAllocatedBw;
    }

    public void setCurrentAllocatedBw(long currentAllocatedBw) {
        this.currentAllocatedBw = currentAllocatedBw;
    }

    public List<Double> getCurrentAllocatedMips() {
        return currentAllocatedMips;
    }

    public void setCurrentAllocatedMips(List<Double> currentAllocatedMips) {
        this.currentAllocatedMips = currentAllocatedMips;
    }

    public boolean isBeingInstantiated() {
        return beingInstantiated;
    }

    public void setBeingInstantiated(boolean beingInstantiated) {
        this.beingInstantiated = beingInstantiated;
    }

    @Override
    public Variable copy() {
        VM vm = new VM();
        vm.setMips(this.getMips());
        vm.setSize(this.getSize());
        vm.setNumberOfPes(this.getNumberOfPes());
        vm.setVmm(this.getVmm());
        vm.setRam(this.getRam());
        vm.setStorage(this.getStorage());
        vm.setBw(this.getBw());
        vm.setPe(this.getPe());
        vm.setDirdyPages(this.getDirdyPages());
        vm.setCurrentAllocatedBw(this.getCurrentAllocatedBw());
        vm.setCurrentAllocatedMips(this.getCurrentAllocatedMips());
        vm.setCurrentAllocatedRam(this.getCurrentAllocatedRam());
        vm.setCurrentAllocatedSize(this.getCurrentAllocatedSize());
        vm.setBeingInstantiated(this.isBeingInstantiated());
        return vm;
    }

    @Override
    public void randomize() { }
}

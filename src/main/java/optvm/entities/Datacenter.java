package optvm.entities;

import java.util.List;

public class Datacenter {

    private String architecture;

    private OS os;

    private String vmm;

    private double timeZone;

    private String country;

    private double costPerSec;

    private double costPerMem;

    private double costPerStorage;

    private double costPerBw;

    private List<Host> hostCons;

    private List<Host> hostMig;

    private Cloud cloud;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getArchitecture() {
        return architecture;
    }

    public void setArchitecture(String architecture) {
        this.architecture = architecture;
    }

    public OS getOs() {
        return os;
    }

    public void setOs(OS os) {
        this.os = os;
    }

    public String getVmm() {
        return vmm;
    }

    public void setVmm(String vmm) {
        this.vmm = vmm;
    }

    public double getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(double timeZone) {
        this.timeZone = timeZone;
    }

    public double getCostPerSec() {
        return costPerSec;
    }

    public void setCostPerSec(double costPerSec) {
        this.costPerSec = costPerSec;
    }

    public double getCostPerMem() {
        return costPerMem;
    }

    public void setCostPerMem(double costPerMem) {
        this.costPerMem = costPerMem;
    }

    public double getCostPerStorage() {
        return costPerStorage;
    }

    public void setCostPerStorage(double costPerStorage) {
        this.costPerStorage = costPerStorage;
    }

    public double getCostPerBw() {
        return costPerBw;
    }

    public void setCostPerBw(double costPerBw) {
        this.costPerBw = costPerBw;
    }

    public List<Host> getHostCons() {
        return hostCons;
    }

    public void setHostCons(List<Host> hostCons) {
        this.hostCons = hostCons;
    }

    public List<Host> getHostMig() {
        return hostMig;
    }

    public void setHostMig(List<Host> hostMig) {
        this.hostMig = hostMig;
    }

    public Cloud getCloud() {
        return cloud;
    }

    public void setCloud(Cloud cloud) {
        this.cloud = cloud;
    }
}

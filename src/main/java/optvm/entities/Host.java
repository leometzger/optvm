package optvm.entities;

import java.util.List;

public class Host {

    private double ram;

    private double storage;

    private double bw;

    private int simultaneousMigrations;

    private int hops;

    private double dirtyPages;

    private int processingUnits;

    private double mips;

    private double eletricCost;

    private double cpu;

    private double pmax;

    private double pmin;

    private boolean active;

    private String vmm;

    private double cram;

    private double cbw;

    private double ccap;

    private double csth;

    private List<VM> vmMig;

    private List<VM> vmActive;

    private Datacenter datacenter;

    public Host() {}

    public double getCram() {
        return cram;
    }

    public void setCram(double cram) {
        this.cram = cram;
    }

    public double getCbw() {
        return cbw;
    }

    public void setCbw(double cbw) {
        this.cbw = cbw;
    }

    public double getCcap() {
        return ccap;
    }

    public void setCcap(double ccap) {
        this.ccap = ccap;
    }

    public double getCsth() {
        return csth;
    }

    public void setCsth(double csth) {
        this.csth = csth;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getBw() {
        return bw;
    }

    public void setBw(double bw) {
        this.bw = bw;
    }

    public double getCapacity() {
        return this.processingUnits * this.mips;
    }

    public double getRam() {
        return ram;
    }

    public void setRam(double ram) {
        this.ram = ram;
    }

    public double getStorage() {
        return storage;
    }

    public void setStorage(double storage) {
        this.storage = storage;
    }

    public int getSimultaneousMigrations() {
        return simultaneousMigrations;
    }

    public void setSimultaneousMigrations(int simultaneousMigrations) {
        this.simultaneousMigrations = simultaneousMigrations;
    }

    public int getHops() {
        return hops;
    }

    public void setHops(int hops) {
        this.hops = hops;
    }

    public double getDirtyPages() {
        return dirtyPages;
    }

    public void setDirtyPages(double dirtyPages) {
        this.dirtyPages = dirtyPages;
    }

    public int getProcessingUnits() {
        return processingUnits;
    }

    public void setProcessingUnits(int processingUnits) {
        this.processingUnits = processingUnits;
    }

    public double getMips() {
        return mips;
    }

    public void setMips(double mips) {
        this.mips = mips;
    }

    public double getEletricCost() {
        return eletricCost;
    }

    public void setEletricCost(double eletricCost) {
        this.eletricCost = eletricCost;
    }

    public double getCpu() {
        return cpu;
    }

    public void setCpu(double cpu) {
        this.cpu = cpu;
    }

    public double getPmax() {
        return pmax;
    }

    public void setPmax(double pmax) {
        this.pmax = pmax;
    }

    public double getPmin() {
        return pmin;
    }

    public void setPmin(double pmin) {
        this.pmin = pmin;
    }

    public List<VM> getVmMig() {
        return vmMig;
    }

    public void setVmMig(List<VM> vmMig) {
        this.vmMig = vmMig;
    }

    public List<VM> getVmActive() {
        return vmActive;
    }

    public void setVmActive(List<VM> vmActive) {
        this.vmActive = vmActive;
    }

    public Datacenter getDatacenter() {
        return datacenter;
    }

    public void setDatacenter(Datacenter datacenter) {
        this.datacenter = datacenter;
    }

    public String getVmm() {
        return vmm;
    }

    public void setVmm(String vmm) {
        this.vmm = vmm;
    }
}

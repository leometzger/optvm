package optvm;

import io.grpc.stub.StreamObserver;

public class OptvmServer extends OptVMGrpc.OptVMImplBase {


    @Override
    public void filterHosts(FilterRequest request, StreamObserver<FilterResponse> responseStreamObserver) {

    }

    @Override
    public void getOptimizedHosts(OptimizationRequest request, StreamObserver<OptmizationResponse> responseObserver) {

    }
}

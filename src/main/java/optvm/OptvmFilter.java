package optvm;

import optvm.constraints.Constraint;
import optvm.constraints.datacenter.DatacenterConstraint;
import optvm.constraints.host.HostConstraint;
import optvm.entities.Cloud;
import optvm.entities.Host;
import optvm.entities.VM;
import optvm.entities.Datacenter;

import java.util.ArrayList;
import java.util.List;

public class OptvmFilter {

    private List<HostConstraint> hostConstraints;

    private List<DatacenterConstraint> dcConstraints;

    private List<Constraint<Cloud>> cloudConstraints;

    public OptvmFilter(){
        this.hostConstraints = new ArrayList();
        this.dcConstraints = new ArrayList();
        this.cloudConstraints = new ArrayList();
    }

    public List<Host> filterHosts(List<Host> hosts, VM vm) {
        List<Host> filteredHosts = new ArrayList();
        for(Host host : hosts) {
            for(HostConstraint constraint : this.hostConstraints) {
                if(!constraint.match(host, vm)) {
                    continue;
                }
            }

            for(DatacenterConstraint constraint : this.dcConstraints) {
                if(!constraint.match(host.getDatacenter())) {
                    continue;
                }
            }
            filteredHosts.add(host);
        }
        return filteredHosts;
    }
}

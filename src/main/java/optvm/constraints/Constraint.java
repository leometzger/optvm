package optvm.constraints;

import optvm.entities.VM;

public interface Constraint<T> {

    boolean match(T value);
}

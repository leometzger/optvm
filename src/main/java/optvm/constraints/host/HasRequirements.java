package optvm.constraints.host;

import optvm.constraints.Constraint;
import optvm.entities.Host;
import optvm.entities.VM;

public class HasRequirements implements HostConstraint {

    @Override
    public boolean match(Host host, VM vm) {
        return vm.getVmm().equals(host.getVmm()) &&
                vm.getBw() <= host.getBw() &&
                vm.getRam() <= host.getRam() &&
                vm.getStorage() <= host.getStorage();
    }
}

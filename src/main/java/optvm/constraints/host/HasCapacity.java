package optvm.constraints.host;

import optvm.constraints.Constraint;
import optvm.entities.Host;
import optvm.entities.VM;

public class HasCapacity implements HostConstraint {

    @Override
    public boolean match(Host value, VM vm) {
        return false;
    }
}

package optvm.constraints.host;

import optvm.entities.Host;
import optvm.entities.VM;

public interface HostConstraint {

    boolean match(Host host, VM vm);
}

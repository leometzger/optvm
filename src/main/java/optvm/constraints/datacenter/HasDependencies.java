package optvm.constraints.datacenter;

import optvm.constraints.Constraint;
import optvm.entities.Datacenter;
import optvm.entities.OS;
import optvm.entities.VM;

public class HasDependencies implements DatacenterConstraint {

    private OS neededOS;

    public HasDependencies() {
       setNeededOS(null);
    }

    @Override
    public boolean match(Datacenter dc) {
        return this.neededOS.equals(dc.getOs());
    }

    public void setNeededOS(OS os) {
        this.neededOS = os;
    }
}

package optvm.constraints.datacenter;

import optvm.constraints.Constraint;
import optvm.entities.Datacenter;
import optvm.entities.Host;
import optvm.entities.VM;

import java.util.ArrayList;
import java.util.List;

public class Conflict implements DatacenterConstraint {

    private VM vm;

    private List<LocationMigration> forbiddenLocalizationMig;

    public Conflict(VM vm) {
        this.forbiddenLocalizationMig = new ArrayList();
        this.vm = vm;
    }

    @Override
    public boolean match(Datacenter dc) {
        String vmCountry = this.vm.getDatacenter().getCountry();

        return this.forbiddenLocalizationMig.stream().noneMatch(lm ->
           lm.origin == vmCountry && lm.dest == dc.getCountry()
        );
    }

    public void addForbiddenLocalizationMig(String from, String to) {
        LocationMigration mig = new LocationMigration(from, to);
        this.forbiddenLocalizationMig.add(mig);
    }

    private class LocationMigration {
        private String origin;
        private String dest;

        public LocationMigration(String origin, String dest) {
            this.origin = origin;
            this.dest = dest;
        }
    }
}

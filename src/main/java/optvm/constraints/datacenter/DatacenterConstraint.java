package optvm.constraints.datacenter;

import optvm.entities.Datacenter;

public interface DatacenterConstraint {

    boolean match(Datacenter datacenter);
}

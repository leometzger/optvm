package optvm.constraints.datacenter;

import optvm.entities.Datacenter;

public class WithinCost implements DatacenterConstraint {

    private double maxPerMem;

    private double maxPerStorage;

    private double maxPerBW;

    private double maxPerSec;

    public WithinCost() {
        this.maxPerBW = 0;
        this.maxPerStorage = 0;
        this.maxPerMem = 0;
        this.maxPerSec = 0;
    }

    public WithinCost(double maxPerMem, double maxPerStorage, double maxPerBW, double maxPerSec) {
        this.maxPerMem = maxPerMem;
        this.maxPerStorage = maxPerStorage;
        this.maxPerBW = maxPerBW;
        this.maxPerSec = maxPerSec;
    }

    @Override
    public boolean match(Datacenter dc) {
        if(this.maxPerBW != 0 && this.maxPerBW < dc.getCostPerBw()) {
            return false;
        } else if(this.maxPerMem != 0 && this.maxPerMem < dc.getCostPerMem()) {
            return false;
        } else if(this.maxPerStorage != 0 && this.maxPerStorage < dc.getCostPerStorage()) {
            return false;
        } else if(this.maxPerSec != 0 && this.maxPerStorage < dc.getCostPerStorage()) {
            return false;
        }
        return true;
    }

    public void setMaxPerMem(double maxPerMem) {
        this.maxPerMem = maxPerMem;
    }

    public void setMaxPerStorage(double maxPerStorage) {
        this.maxPerStorage = maxPerStorage;
    }

    public void setMaxPerBW(double maxPerBW) {
        this.maxPerBW = maxPerBW;
    }

    public double getMaxPerSec() {
        return maxPerSec;
    }

    public void setMaxPerSec(double maxPerSec) {
        this.maxPerSec = maxPerSec;
    }
}

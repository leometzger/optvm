package optvm.constraints.host;

import optvm.entities.Host;
import optvm.entities.VM;
import org.junit.Before;
import org.junit.Test;

import static optvm.constraints.Space.GB;
import static org.junit.Assert.*;

public class HasRequirementsTest {

    private HasRequirements hasRequirements;

    private Host host;

    private VM vm;

    @Before
    public void setup() {
        this.vm = new VM();
        this.vm.setVmm("XEN");
        this.vm.setBw(2000);
        this.vm.setStorage(500 * GB);
        this.vm.setRam(8 * GB);

        this.hasRequirements = new HasRequirements();

        this.host = new Host();
        this.host.setVmm("XEN");
        this.host.setBw(5000);
        this.host.setStorage(550 * GB);
        this.host.setRam(8 * GB);
    }

    @Test
    public void testShouldMatch() {
        assertTrue(this.hasRequirements.match(this.host, this.vm));
    }

    @Test
    public void testShouldNotMatchByVMM() {
        this.host.setVmm("QEMU");
        assertFalse(this.hasRequirements.match(this.host, this.vm));
    }

    @Test
    public void testShouldNotMatchByBW() {
        this.host.setBw(1000);
        assertFalse(this.hasRequirements.match(this.host, this.vm));
    }

    @Test
    public void testShouldNotMatchByStorage() {
        this.host.setStorage(300 * GB);
        assertFalse(this.hasRequirements.match(this.host, this.vm));
    }

    @Test
    public void testShouldNotMatchByRAM() {
        this.host.setRam(4 * GB);
        assertFalse(this.hasRequirements.match(this.host, this.vm));
    }
}
package optvm.constraints.datacenter;

import optvm.entities.Datacenter;
import optvm.entities.OS;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HasDependenciesTest {

    private HasDependencies hasDependencies;

    @Before
    public void setup() {
        this.hasDependencies = new HasDependencies();
        hasDependencies.setNeededOS(OS.NIX);
    }

    @Test
    public void testShouldMatch() {
        Datacenter dc = new Datacenter();
        dc.setOs(OS.NIX);

        assertTrue(this.hasDependencies.match(dc));
    }

    @Test
    public void testShouldNotMatch() {
        Datacenter dc = new Datacenter();
        dc.setOs(OS.WINDOWS);

        assertFalse(this.hasDependencies.match(dc));
    }
}
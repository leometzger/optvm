package optvm.constraints.datacenter;

import optvm.entities.Datacenter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WithinCostTest {

    private WithinCost withinCostConstraint;

    private Datacenter datacenter;

    @Before
    public void setup() {
        this.withinCostConstraint = new WithinCost(0.5, 0.04, 0.95, 0.005);

        this.datacenter = new Datacenter();
        this.datacenter.setCostPerMem(0.4);
        this.datacenter.setCostPerStorage(0.03);
        this.datacenter.setCostPerBw(0.9);
        this.datacenter.setCostPerSec(0.4);
    }

    @Test
    public void testShouldMatch() {
        assertTrue(this.withinCostConstraint.match(this.datacenter));
    }

    @Test
    public void testShouldMatchWithPartialValues() {
       WithinCost withinCostConstraint = new WithinCost();
       withinCostConstraint.setMaxPerStorage(0.5);

       Datacenter dcOne = new Datacenter();
       dcOne.setCostPerStorage(0.6);

       Datacenter dcTwo = new Datacenter();
       dcTwo.setCostPerStorage(0.1);

       assertFalse(withinCostConstraint.match(dcOne));
       assertTrue(withinCostConstraint.match(dcTwo));
    }

    public void testShouldNotMatchByMem() {
        this.datacenter.setCostPerMem(0.5);
        assertFalse(this.withinCostConstraint.match(this.datacenter));
    }

    public void testShouldNotMatchByBW() {
        this.datacenter.setCostPerBw(0.1);
        assertFalse(this.withinCostConstraint.match(this.datacenter));
    }

    public void testShouldNotMatchBySec() {
        this.datacenter.setCostPerSec(0.1);
        assertFalse(this.withinCostConstraint.match(this.datacenter));
    }

    public void testShouldNotMatchByStorage() {
        this.datacenter.setCostPerStorage(0.1);
        assertFalse(this.withinCostConstraint.match(this.datacenter));
    }
}

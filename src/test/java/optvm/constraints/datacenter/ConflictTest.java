package optvm.constraints.datacenter;

import optvm.entities.Datacenter;
import optvm.entities.VM;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConflictTest {

    private Conflict conflict;

    private VM vm;

    @Before
    public void setup() {
        Datacenter vmdc = new Datacenter();
        vmdc.setCountry("USA");

        this.vm = new VM();
        this.vm.setDatacenter(vmdc);

        this.conflict = new Conflict(this.vm);
        this.conflict.addForbiddenLocalizationMig("USA", "BR");
        this.conflict.addForbiddenLocalizationMig("USA", "ISR");
    }

    @Test
    public void testShouldMatch() {
        Datacenter dc = new Datacenter();
        dc.setCountry("USA");

        assertTrue(this.conflict.match(dc));
    }

    @Test
    public void testShouldNotMatch() {
        Datacenter dcOne = new Datacenter();
        dcOne.setCountry("ISR");
        Datacenter dcTwo = new Datacenter();
        dcTwo.setCountry("BR");

        assertFalse(this.conflict.match(dcOne));
        assertFalse(this.conflict.match(dcTwo));
    }
}
package optvm.objectives;

import optvm.constraints.Space;
import optvm.entities.VM;
import optvm.entities.Host;
import optvm.objectives.Equations;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class EquationsTest {

    private Equations equations;

    @Before
    public void setup(){
        this.equations = new Equations();
    }

    @Test
    public void shouldCalculateTraffic() {
        Host host = new Host();
        host.setHops(1000);
        VM vm = new VM();
        vm.setSize(10000);
        double processingFactor = 0.1;

        double result = equations.calculateTraffic(host, vm, processingFactor);

        assertEquals(110.0, result, 0.0);
    }

    @Test
    public void shouldCalculateEnergy() {
        Host hostOne = new Host();
        hostOne.setCpu(50.0);
        hostOne.setPmax(60.0);
        hostOne.setPmin(5.0);
        hostOne.setActive(true);

        Host hostTwo = new Host();
        hostTwo.setCpu(25.0);
        hostTwo.setPmax(70.0);
        hostTwo.setPmin(10.0);
        hostTwo.setActive(true);

        Host hostThree = new Host();
        hostThree.setCpu(10.0);
        hostThree.setPmax(40.0);
        hostThree.setPmin(2.0);
        hostThree.setActive(true);

        List<Host> hosts = Arrays.asList(hostOne, hostTwo, hostThree);

        assertEquals(29510, equations.calculateEnergy(hosts), 0.0);
    }

    @Test
    public void shouldBeZeroWithInactiveHosts() {
        Host hostOne = new Host();
        hostOne.setCpu(50.0);
        hostOne.setPmax(60.0);
        hostOne.setPmin(5.0);
        hostOne.setActive(false);

        Host hostTwo = new Host();
        hostTwo.setCpu(25.0);
        hostTwo.setPmax(70.0);
        hostTwo.setPmin(10.0);
        hostTwo.setActive(false);

        List<Host> hosts = Arrays.asList(hostOne, hostTwo);

        assertEquals(0, equations.calculateEnergy(hosts), 0.0);
    }

    @Test
    public void shouldCalculateVolume() {
        Host host = new Host();
        host.setCcap(0.5);
        host.setCram(0.3);
        host.setCbw(0.4);
        host.setCsth(0.2);

        double result = equations.calculateVolume(host);

        assertEquals(5.952, result, 0.001);
    }

    @Test
    public void shouldCalculateCapacity() {
        Host host = new Host();
        host.setMips(1000);
        host.setProcessingUnits(3);

        double result = equations.calculateCapacity(host);

        assertEquals(3000, result, 0.0);
    }

    @Test
    public void shouldCalculateFixationTime() {
        VM vmOne = new VM();
        vmOne.setCurrentAllocatedRam(1 * Space.GB);
        VM vmTwo = new VM();
        vmOne.setCurrentAllocatedRam(500 * Space.MB);
        List<VM> activeVMs = Arrays.asList(vmOne, vmTwo);

        VM vmThree = new VM();
        vmThree.setCurrentAllocatedRam(2 * Space.GB);
        VM vmFour = new VM();
        vmThree.setCurrentAllocatedRam(1 * Space.GB);
        List<VM> migVMs = Arrays.asList(vmOne, vmTwo);


        Host host = new Host();
        host.setBw(1000 * Space.MB);
        host.setVmActive(activeVMs);
        host.setVmMig(migVMs);

        double result = equations.calculateFixationTime(host);

        assertEquals(1.0, result, 0.0);
    }
}